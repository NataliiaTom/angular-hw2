import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommentsWrapper } from 'src/app/app.model';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent {

  @Input() public id = '';
  @Input() public like = 0;
  public commentNum = 0;
  @Input() public comments: CommentsWrapper = [];
  public commentAvailable = false;
  public commentText: string;
  public pencil = false;
  public taskText: string;
  @Input() public tasks: CommentsWrapper = [];

  @Output() likeClicked = new EventEmitter<any>();

  constructor() { }

  addLike(): void {
    this.like = Number(this.like) + Number(1);
    this.likeClicked.emit({
      id: this.id,
      like: this.like,
      tasks: this.tasks,
      comments: this.comments
    });
  }

  commentIsPossible(): void {
    this.commentAvailable = !this.commentAvailable;
  }

  onEnter(): void | boolean {

    if (this.commentText.length > 0) {
      this.comments.push({ text: this.commentText, id: new Date().getUTCMilliseconds() });
      this.commentNum = this.commentNum + 1;
      this.commentIsPossible();
      this.commentText = '';
    }
    return false;
  }

  deleteComment(Id): void {
    const index = this.comments.map(x => {
      return x.id;
    }).indexOf(Id);

    this.comments.splice(index, 1);
  }

  addTask(): void {
    this.pencil = !this.pencil;
  }

  taskAdded(): void {
    this.tasks.push({ text: this.taskText, id: new Date().getUTCMilliseconds() });
    this.taskText = '';
  }
}
