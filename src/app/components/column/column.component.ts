import { Component, OnInit } from '@angular/core';
import { CardComponent } from '../card/card.component';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.css']
})
export class ColumnComponent implements OnInit {

  public columns: Array<string> = [];
  columnTitle: string;

  addColumn(): void {
    this.columns.push(this.columnTitle);
    this.columnTitle = '';
  }

  constructor() { }

  ngOnInit(): void {
  }

}
