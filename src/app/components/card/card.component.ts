import { Component, Input, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { CardWrapper } from 'src/app/app.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() title;
  public cards: CardWrapper = [];
  public cardTitle = 'Card';


  constructor() { }

  public handleLikeCLicked(e): void {
    console.warn(e);
    const { id, like } = e;
    const card = this.cards.find(x => x.id === id);
    const index = this.cards.indexOf(card);
    this.cards[index] = e;
  }

  drop(event: CdkDragDrop<{ id: number, like: number }[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  addCard(): void {
    this.cards.push({
      id: new Date().getUTCMilliseconds(),
      like: 0,
      tasks: [],
      comments: []
    });
  }


}
