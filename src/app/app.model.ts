export interface Card {
    id: number;
    like: number;
    tasks: Array<{ text: string, id: number }>;
    comments: Array<{ text: string, id: number }>;
}
export interface CardWrapper extends Array<Card> { }



export interface Comments {
    text: string;
    id: number;
}
export interface CommentsWrapper extends Array<Comments> { }
